/*
 * Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <math.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <assert.h>
#include "libtct.h"
#include "variant.h"

#define TCT_BID_ASK_SPREAD                   ("TCT_BID_ASK_SPREAD")

#define BOND_PROVIDER_CODE_SIZE 	6

typedef struct optimal_provider {
	char bond_provider[ BOND_PROVIDER_CODE_SIZE ];
	variant_t value;
} optimal_provider_t;


typedef boolean (*field_dependencies) ( const security_t* p_provider );
typedef boolean (*field_initializer)  ( const security_t* p_bond );
typedef boolean (*field_calculate)    ( const security_t* p_provider, security_t* p_bond );//, optimal_provider_t *p_optimal_provider );

typedef struct tct_field_descriptor {
	const char*        mnemonic;
	unsigned char      type;
	field_dependencies dependencies;
	field_calculate    calculate;
	const char*        description;
} tct_field_descriptor_t;

static int field_descriptor_compare( const void *p_left, const void *p_right );
static tct_field_descriptor_t* find_field_descriptor( const char* field );

/*
 *    Field Dependency Functions
 */
static boolean best_ask_dependencies  ( const security_t* p_provider );
static boolean best_bid_dependencies  ( const security_t* p_provider );

/*
 *    Field Initializer Functions
 */
static boolean best_ask_init  ( const security_t* p_provider );
static boolean best_bid_init  ( const security_t* p_provider );

/*
 *    Field Calculation Functions
 */
static boolean best_ask               ( const security_t* p_provider, security_t* p_bond );
static boolean max_ask_size           ( const security_t* p_provider, security_t* p_bond );
static boolean min_ask_size           ( const security_t* p_provider, security_t* p_bond );
static boolean best_bid               ( const security_t* p_provider, security_t* p_bond );
static boolean max_bid_size           ( const security_t* p_provider, security_t* p_bond );
static boolean min_bid_size           ( const security_t* p_provider, security_t* p_bond );
static boolean best_bid_ask_spread    ( const security_t* p_provider, security_t* p_bond );

static tct_field_descriptor_t FIELDS[] = {
	{ "TCT_BEST_ASK",               VARIANT_DECIMAL,          NULL,                           best_ask,                "Best Ask Price" },
	{ "TCT_BEST_ASK_PROVIDER",      VARIANT_DECIMAL,          NULL,                           NULL,                    "The provider with the best ask price." },
	{ "TCT_BEST_BID",               VARIANT_DECIMAL,          NULL,                           best_bid,                "Best Bid Price" },
	{ "TCT_BEST_BID_ASK_SPREAD",    VARIANT_DECIMAL,          NULL,                           best_bid_ask_spread,     "Best Bid-Ask Spread" },
	{ "TCT_BEST_BID_PROVIDER",      VARIANT_DECIMAL,          NULL,                           NULL,                    "The provider with the best bid price." },
	{ "TCT_MAX_ASK_SIZE",           VARIANT_UNSIGNED_INTEGER, NULL,                           max_ask_size,            "Maximum Ask Size" },
	{ "TCT_MAX_ASK_SIZE_PROVIDER",  VARIANT_DECIMAL,          NULL,                           NULL,                    "The provider with the maximum ask size." },
	{ "TCT_MAX_BID_SIZE",           VARIANT_UNSIGNED_INTEGER, NULL,                           max_bid_size,            "Maximum Bid Size" },
	{ "TCT_MAX_BID_SIZE_PROVIDER",  VARIANT_DECIMAL,          NULL,                           NULL,                    "The provider with the maximum bid size." },
	{ "TCT_MIN_ASK_SIZE",           VARIANT_UNSIGNED_INTEGER, NULL,                           min_ask_size,            "Minimum Ask Size" },
	{ "TCT_MIN_ASK_SIZE_PROVIDER",  VARIANT_DECIMAL,          NULL,                           NULL,                    "The provider with the minimum ask size." },
	{ "TCT_MIN_BID_SIZE",           VARIANT_UNSIGNED_INTEGER, NULL,                           min_bid_size,            "Minimum Bid Size" },
	{ "TCT_MIN_BID_SIZE_PROVIDER",  VARIANT_DECIMAL,          NULL,                           NULL,                    "The provider with the minimum bid size." },
};

#define TCT_FIELD_COUNT       (sizeof(FIELDS) / sizeof(tct_field_descriptor_t))



void tct_reset_fields( subscription_t* p_bond_providers, subscription_t* p_bonds )//, const char** fields, unsigned short number_of_fields )
{
	security_t* p_bond;
	for( p_bond = subscription_first_security( p_bonds );
		 p_bond != NULL;
		 p_bond = subscription_next_security( p_bonds ) )
	{
		best_ask_init( p_bond );
		best_bid_init( p_bond );
	}
}


void tct_compute_fields( subscription_t* p_bond_providers, subscription_t* p_bonds )//, const char** fields, unsigned short number_of_fields )
{
	security_t* p_bond_provider;
	security_t* p_bond;
	char bond[ 32 ];
	char ticker[ 64 ];

	for( p_bond_provider = subscription_first_security( p_bond_providers );
		 p_bond_provider != NULL;
		 p_bond_provider = subscription_next_security( p_bond_providers ) )
	{
		const char* provider = security_ticker( p_bond_provider );
		char *at_symbol = strchr(provider, '@');
		size_t len = at_symbol - provider;
		strncpy( bond, provider, len );
		bond[ len ] = '\0';

		_snprintf( ticker, sizeof(ticker), "%s Corp", bond );

		p_bond = subscription_security( p_bonds, ticker );

		if( !p_bond ) continue;

		best_ask( p_bond_provider, p_bond );
		min_ask_size( p_bond_provider, p_bond );
		max_ask_size( p_bond_provider, p_bond );

		best_bid( p_bond_provider, p_bond );
		min_bid_size( p_bond_provider, p_bond );
		max_bid_size( p_bond_provider, p_bond );

		best_bid_ask_spread( p_bond_provider, p_bond );
	}
}

const char* tct_field_description( const char* field )
{
	tct_field_descriptor_t* p_field_descriptor = find_field_descriptor( field );

	return p_field_descriptor ? p_field_descriptor->description : "Unknown TCT Field";
}

tct_field_descriptor_t* find_field_descriptor( const char* field )
{
	tct_field_descriptor_t key;
	tct_field_descriptor_t *p_field_descriptor = NULL;

	key.mnemonic = field;

	p_field_descriptor = (tct_field_descriptor_t *) bsearch( &key, FIELDS, TCT_FIELD_COUNT, sizeof(tct_field_descriptor_t), field_descriptor_compare );	

	return p_field_descriptor;
}

int field_descriptor_compare( const void *p_left, const void *p_right )
{
	const tct_field_descriptor_t *p_left_des;
	const tct_field_descriptor_t *p_right_des;
	p_left_des  = (tct_field_descriptor_t *) p_left;	
	p_right_des = (tct_field_descriptor_t *) p_right;	

	return strncmp( p_left_des->mnemonic, p_right_des->mnemonic, strlen(p_left_des->mnemonic) );
}


boolean best_ask_dependencies( const security_t* p_provider )
{
	boolean has_ask      = security_has_field( p_provider, "ASK" );
	boolean has_ask_size = security_has_field( p_provider, "ASK_SIZE" );
	boolean has_provider = security_has_field( p_provider, "RT_PRICING_SOURCE" );

	return has_ask && has_ask_size && has_provider;
}

boolean best_bid_dependencies( const security_t* p_provider )
{
	boolean has_bid      = security_has_field( p_provider, "BID" );
	boolean has_bid_size = security_has_field( p_provider, "BID_SIZE" );
	boolean has_provider = security_has_field( p_provider, "RT_PRICING_SOURCE" );

	return has_bid && has_bid_size && has_provider;
}

boolean best_ask_init( const security_t* p_bond )
{
	security_set_field_value_as_decimal( p_bond, "TCT_BEST_ASK", DBL_MAX );
	security_set_field_value_as_uinteger( p_bond, "TCT_MAX_ASK_SIZE", 0L );
	security_set_field_value_as_uinteger( p_bond, "TCT_MIN_ASK_SIZE", ULONG_MAX );
}

boolean best_bid_init( const security_t* p_bond )
{
	security_set_field_value_as_decimal( p_bond, "TCT_BEST_BID", 0.0 );
	security_set_field_value_as_uinteger( p_bond, "TCT_MAX_BID_SIZE", 0L );
	security_set_field_value_as_uinteger( p_bond, "TCT_MIN_BID_SIZE", 0L );
}


boolean best_ask( const security_t* p_provider, security_t* p_bond )
{
	unsigned long ask_size;
	double ask;
	double best_ask;

	assert( p_provider );
	assert( p_bond );

	ask_size = security_field_value_as_uinteger( p_provider, "ASK_SIZE" );
	ask      = security_field_value_as_decimal( p_provider, "ASK" );
	best_ask = security_field_value_as_decimal( p_bond, "TCT_BEST_ASK" );

	if( ask_size > 0 && (ask < best_ask || !security_has_field(p_bond, "TCT_BEST_ASK")) )
	{
		const char* provider = security_field_value_as_string( p_provider, "RT_PRICING_SOURCE" );

		security_set_field_value_as_decimal( p_bond, "TCT_BEST_ASK", ask );
		security_set_field_value_as_string( p_bond, "TCT_BEST_ASK_PROVIDER", provider );
	}

	return TRUE;
}

boolean max_ask_size( const security_t* p_provider, security_t* p_bond )
{
	unsigned long ask_size;
	unsigned long max_ask_size;

	assert( p_provider );
	assert( p_bond );

	ask_size     = security_field_value_as_uinteger( p_provider, "ASK_SIZE" );
	max_ask_size = security_field_value_as_uinteger( p_bond, "TCT_MAX_ASK_SIZE" );

	if( ask_size > 0 && (ask_size > max_ask_size || !security_has_field(p_bond, "TCT_MAX_ASK_SIZE")) )
	{
		const char* provider = security_field_value_as_string( p_provider, "RT_PRICING_SOURCE" );

		security_set_field_value_as_uinteger( p_bond, "TCT_MAX_ASK_SIZE", ask_size );
		security_set_field_value_as_string( p_bond, "TCT_MAX_ASK_SIZE_PROVIDER", provider );
	}

	return TRUE;
}

boolean min_ask_size( const security_t* p_provider, security_t* p_bond )
{
	unsigned long ask_size;
	unsigned long min_ask_size;

	assert( p_provider );
	assert( p_bond );

	ask_size     = security_field_value_as_uinteger( p_provider, "ASK_SIZE" );
	min_ask_size = security_field_value_as_uinteger( p_bond, "TCT_MIN_ASK_SIZE" );

	if( ask_size > 0 && (ask_size < min_ask_size || !security_has_field(p_bond, "TCT_MIN_ASK_SIZE")) )
	{
		const char* provider = security_field_value_as_string( p_provider, "RT_PRICING_SOURCE" );

		security_set_field_value_as_uinteger( p_bond, "TCT_MIN_ASK_SIZE", ask_size );
		security_set_field_value_as_string( p_bond, "TCT_MIN_ASK_SIZE_PROVIDER", provider );
	}

	return TRUE;
}


boolean best_bid( const security_t* p_provider, security_t* p_bond )
{
	unsigned long bid_size;
	double bid;
	double best_bid;

	assert( p_provider );
	assert( p_bond );

	bid_size = security_field_value_as_uinteger( p_provider, "BID_SIZE" );
	bid      = security_field_value_as_decimal( p_provider, "BID" );
	best_bid = security_field_value_as_decimal( p_bond, "TCT_BEST_BID" );

	if( bid_size > 0 && (bid > best_bid || !security_has_field(p_bond, "TCT_BEST_BID")) )
	{
		const char* provider = security_field_value_as_string( p_provider, "RT_PRICING_SOURCE" );

		security_set_field_value_as_decimal( p_bond, "TCT_BEST_BID", bid );
		security_set_field_value_as_string( p_bond, "TCT_BEST_BID_PROVIDER", provider );
	}

	return TRUE;
}

boolean max_bid_size( const security_t* p_provider, security_t* p_bond )
{
	unsigned long bid_size;
	unsigned long max_bid_size;
	
	assert( p_provider );
	assert( p_bond );

	bid_size     = security_field_value_as_uinteger( p_provider, "BID_SIZE" );
	max_bid_size = security_field_value_as_uinteger( p_bond, "TCT_MAX_BID_SIZE" );

	if( bid_size > 0 && (bid_size > max_bid_size || !security_has_field(p_bond, "TCT_MAX_BID_SIZE")) )
	{
		const char* provider = security_field_value_as_string( p_provider, "RT_PRICING_SOURCE" );

		security_set_field_value_as_uinteger( p_bond, "TCT_MAX_BID_SIZE", bid_size );
		security_set_field_value_as_string( p_bond, "TCT_MAX_BID_SIZE_PROVIDER", provider );
	}

	return TRUE;
}

boolean min_bid_size( const security_t* p_provider, security_t* p_bond )
{
	unsigned long bid_size;
	unsigned long min_bid_size;
	
	assert( p_provider );
	assert( p_bond );

	bid_size     = security_field_value_as_uinteger( p_provider, "BID_SIZE" );
	min_bid_size = security_field_value_as_uinteger( p_bond, "TCT_MIN_BID_SIZE" );

	if( bid_size > 0 && (bid_size < min_bid_size || !security_has_field(p_bond, "TCT_MIN_BID_SIZE")) )
	{
		const char* provider = security_field_value_as_string( p_provider, "RT_PRICING_SOURCE" );

		security_set_field_value_as_uinteger( p_bond, "TCT_MIN_BID_SIZE", bid_size );
		security_set_field_value_as_string( p_bond, "TCT_MIN_BID_SIZE_PROVIDER", provider );
	}

	return TRUE;
}

boolean best_bid_ask_spread( const security_t* p_provider, security_t* p_bond )
{
	double best_bid;
	double best_ask;

	assert( p_provider );
	assert( p_bond );

	best_bid = security_field_value_as_decimal( p_bond, "TCT_BEST_BID" );
	best_ask = security_field_value_as_decimal( p_bond, "TCT_BEST_ASK" );
		
	security_set_field_value_as_decimal( p_bond, "TCT_BEST_BID_ASK_SPREAD", fabs(best_bid - best_ask) );

	return TRUE;
}
