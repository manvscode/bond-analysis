/*
 * Copyright (C) 2011 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <blpapi_service.h>

//#ifndef NDEBUG
#include "bench-mark.h"
#include "variant.h"
#include "vector.h"
//#endif
#include "libblp.h"

const char *stocks[] = {
	"IBM US Equity",
	"MSFT US Equity",
	"GOOG US Equity",
};

const char *bonds[] = {
	"USP3772WAC66",
	NULL,
	"US912828RR30",
	"ED642614",
	"EC219146",
	"EI224092",
	"EC359050",
	"ED801796",
	"TT333060",
	"EC232402",
	"ED289312",
	"EF238767",
	"EC283192",
	"EH995062",
	"EC503543",
	"EC816262",
	"EH722075",
	"EC880518",
	"EI693746",
	"ED040206",
	"ED608400",
	"EI543131",
	"EF742979",
	"EG988573",
	"ED249704",
	"EH713213",
	"EI019448",
	"EI543171",
	"EI021953",
	"EI543179",
	"EI758313",
	"EG462878",
	"EI410308",
	NULL,
};

const char *providers[] = {
	"BART",
	"DB",
	"UBST",
	"BANI",
	NULL,
	"BCAP",
	"BIFI",
	"CRFQ",
	"GSEM",
	"GSLM",
	"HAPX",
	"MAXM",
	"PCTF",
	"RBCX",
	"RBSM",
	"SBEM",
	"SETA",
	"SUSF",
	"USEM",
	"VOTO",
	"ZKB",
	NULL,
};
static void print_fields( security_t* p_security );
static void print_field( const security_t *p_security, const char *fld );
static boolean ticker_destroy( void *p_ticker );
static void reference_data_test( const char* ticker );
static void market_data_test( );
	
static blp_t *blp;

#ifdef NDEBUG
#define BENCH_MARK_START( desc ) 
#define BENCH_MARK_END( ) 
#else
#define BENCH_MARK_START( desc ) {\
	bench_mark_t bm; \
	bm = bench_mark_create( desc ); \
	bench_mark_start( bm ); 

#define BENCH_MARK_END( ) \
	bench_mark_end( bm ); \
	bench_mark_report( bm ); \
	bench_mark_destroy( bm ); }
#endif

int main( int argc, char *argv[] )
{
	blp = blp_create( BLP_DEFAULT_HOST, BLP_DEFAULT_PORT );

	if( !blp )
	{
		return 1;
	}

#if 1
	size_t i, j;

	BENCH_MARK_START( "Reference Data Service Test" );
	for( i = 0; bonds[ i ] != NULL; i++ )
	{
		for( j = 0; providers[ j ] != NULL; j++ )
		{
			char ticker[128];

			sprintf( ticker, "%s@%s corp", bonds[i], providers[j] );

			reference_data_test( ticker );
		}
	}
	BENCH_MARK_END( );	
#else
	BENCH_MARK_START( "Market Data Test" )
	market_data_test( );
	BENCH_MARK_END( );
#endif
	
	blp_destroy( blp );

	return 0;
}

void print_fields( security_t* p_security )
{
	const char* field = security_first_field( p_security );
	if( field )
	{
		printf( "%45s: ", "Fields" );
	}

	while( field )
	{
		printf( "%s", field );
		field = security_next_field( p_security );
		if( field )
		{
			printf( ", " );
		}
		else
		{
			printf("\n");
		}
	}
}

void print_field( const security_t *p_security, const char* fld )
{	
	if( security_has_field( p_security, fld ) )
	{
		unsigned short type = security_field_type( p_security, fld );

		switch( type )
		{
		case BLP_FIELD_TYPE_DECIMAL:
			printf( "%45s: %.2lf\n", blp_field_description(fld), security_field_value_as_decimal(p_security, fld) );
			break;
		case BLP_FIELD_TYPE_INTEGER:
			printf( "%45s: %ld\n", blp_field_description(fld), security_field_value_as_integer(p_security, fld) );
			break;
		case BLP_FIELD_TYPE_UNSIGNED_INTEGER:
			printf( "%45s: %lu\n", blp_field_description(fld), security_field_value_as_uinteger(p_security, fld) );
			break;
		case BLP_FIELD_TYPE_STRING:
			printf( "%45s: %s\n", blp_field_description(fld), security_field_value_as_string(p_security, fld) );
			break;
		default:
			printf( "%45s: -- \n", blp_field_description(fld) );
			break;
		}
	}
}

static const char *fields[] = {
	"SECURITY_DES",
	"BID",
	"ASK",
	"BID_SIZE",
	"ASK_SIZE",
	"LAST_UPDATE",
};

void reference_data_test( const char* ticker )
{
	security_t *security;
	boolean result = FALSE;

	security = security_create( );
	//US298785EV42@bart corp
	result = blp_reference_data( blp, security, ticker, sizeof(fields) / sizeof(char*), fields );
	


	if( result )
	{
		printf( "%45s: %s\n", "Bond", security_ticker( security ) );
		#if 0
		print_fields( security );
		#endif

		for( const char* field = security_first_field( security ); 
		     field != NULL; 
			 field = security_next_field( security ) )
		{
			print_field( security, field );
		}

		security_destroy( security );

		printf( "-------------------------------------------------------------\n" );
	}
	else
	{
		printf( "Reference Data Request failed because: %s\n", blp_error(blp) );
	}
}


void market_data_test( )
{
	boolean result;
	subscription_t *subscription;
	size_t i, j;
	pvector_t bonds_vec;

	pvector_create( &bonds_vec, 100, ticker_destroy );


	for( i = 0; bonds[ i ] != NULL; i++ )
	{
		for( j = 0; providers[ j ] != NULL; j++ )
		{
			char ticker[128];

			sprintf( ticker, "%s@%s corp", bonds[i], providers[j] );
			//sprintf( ticker, "%s corp", bonds[i] );

			printf( "Subscribing to %s...\n", ticker );
			pvector_push( &bonds_vec, strdup(ticker) );
		}
	}

	subscription = subscription_create( );
	subscription_set_interval( subscription, 1 );
	
	result = blp_market_data( blp, subscription, (const char **) pvector_array(&bonds_vec), pvector_size(&bonds_vec), fields, sizeof(fields) / sizeof(char*) );


	printf( "Waiting for data...\n" );


	while( TRUE )
	{
		for( i = 0; i < pvector_size(&bonds_vec); i++ )
		{
			const char* ticker = (const char*) pvector_get( &bonds_vec, i );

			if( subscription_has_security(subscription, ticker) )
			{
				security_t* security = subscription_security( subscription, ticker );

				assert( security );

				if( !security_has_field( security, "ASK" ) )
				{
					continue;
				}

				#if 0
				if( !security_has_field( security, "ID_ISIN" ) || !security_has_field( security, "ID_CUSIP" ) || !security_has_field( security, "SECURITY_DES" ) )
				{
					blp_reference_data_v( blp, security, security_ticker(security), 3, "ID_CUSIP", "ID_ISIN", "SECURITY_DES" );
				}
				#endif
				

				printf( "%45s: %s\n", "Bond", security_ticker( security ) );
				#if 0
				print_fields( (security_t*) security );
				#endif

				for( const char* field = security_first_field( (security_t*) security ); 
					 field != NULL; 
					 field = security_next_field( (security_t*) security ) )
				{
					print_field( security, field );
				}

				printf( "-------------------------------------------------------------\n" );
			}
		}
		_sleep( 2 * 1000 );
	}

	subscription_destroy( subscription );
	pvector_destroy( &bonds_vec );
}

boolean ticker_destroy( void *p_ticker )
{
	free( p_ticker );
	return TRUE;
}

