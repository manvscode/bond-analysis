﻿/*
 * Copyright (C) 2011 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BondAnalysis.AppData;

namespace BondAnalysis
{
    public partial class BondsProviderDlg : Form
    {
        protected System.Collections.Generic.List<BondProviderConfig> bondsProviders;

        public BondsProviderDlg()
        {
            InitializeComponent();
            this.bondsProviders = new List<BondProviderConfig>();
        }

        public List<BondProviderConfig> Providers
        {
            get
            {
                return this.bondsProviders;
            }
            set
            {
                this.bondsProviders = value;
            }
        }

        new public DialogResult ShowDialog()
        {
            foreach (BondProviderConfig bond in this.bondsProviders)
            {
                this.addBondRow(bond);
            }

            DialogResult result = base.ShowDialog();

            this.bondsProviders.Clear();

            foreach (DataGridViewRow row in this.bondsProvidersList.Rows)
            {
                if (row == null || row.Cells["bondsProvidersProviderColumn"].Value == null) continue;

                BondProviderConfig bc = new BondProviderConfig();
                bc.Provider = row.Cells["bondsProvidersProviderColumn"].Value.ToString();

                if (row.Cells["bondsProvidersNotesColumn"].Value != null)
                {
                    bc.Notes = row.Cells["bondsProvidersNotesColumn"].Value.ToString();
                }
                this.bondsProviders.Add(bc);
            }

            return result;
        }

        protected void addBondRow(BondProviderConfig bond)
        {
            //DataGridViewRow row = new DataGridViewRow();
            int rowIndex = this.bondsProvidersList.Rows.Add();
            DataGridViewRow row = this.bondsProvidersList.Rows[rowIndex];
            row.Cells["bondsProvidersProviderColumn"].Value = bond.Provider;
            row.Cells["bondsProvidersNotesColumn"].Value = bond.Notes;
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in this.bondsProvidersList.SelectedCells)
            {
                try
                {
                    this.bondsProvidersList.Rows.RemoveAt(cell.RowIndex);
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            BondProviderConfig bf = new BondProviderConfig();
            bf.Provider = "** New Bond **";
            this.addBondRow(bf);
        }

        private void bondsProvidersList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                int columnIndex = e.ColumnIndex;
                int rowIndex = e.RowIndex;

                DataGridViewRow row = this.bondsProvidersList.Rows[rowIndex];

                string bond = string.Empty;

                if (row.Cells[columnIndex].Value != null)
                {
                    if (row.Cells[columnIndex] is DataGridViewTextBoxCell)
                    {
                        bond = row.Cells[columnIndex].Value.ToString().ToUpper();
                    }
                    else if (row.Cells[columnIndex] is DataGridViewComboBoxCell)
                    {
                        bond = ((DataGridViewComboBoxCell)row.Cells[columnIndex]).FormattedValue.ToString().ToUpper();
                    }
                }

                row.Cells[columnIndex].Value = bond; // made uppercase
            }
        }

        private void bondsProvidersList_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridViewRow row = this.bondsProvidersList.Rows[e.RowIndex];

            if (e.RowIndex % 2 == 0)
            {
                row.DefaultCellStyle.BackColor = Color.FromArgb(245, 245, 250);
            }
            else
            {
                row.DefaultCellStyle.BackColor = Color.White;
            }
        }


        private Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;

        private void bondsProvidersList_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (this.dragBoxFromMouseDown != Rectangle.Empty &&
                !this.dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = this.bondsProvidersList.DoDragDrop(
                    this.bondsProvidersList.Rows[this.rowIndexFromMouseDown],
                    DragDropEffects.Move);
                }
            }
        }

        private void bondsProvidersList_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            this.rowIndexFromMouseDown = this.bondsProvidersList.HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                // Remember the point where the mouse down occurred. 
                // The DragSize indicates the size that the mouse can move 
                // before a drag event should be started.                
                Size dragSize = SystemInformation.DragSize;
                // Create a rectangle using the DragSize, with the mouse position being
                // at the center of the rectangle.
                this.dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                e.Y - (dragSize.Height / 2)),
                dragSize);
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                this.dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void bondsProvidersList_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void bondsProvidersList_DragDrop(object sender, DragEventArgs e)
        {
            // The mouse locations are relative to the screen, so they must be 
            // converted to client coordinates.
            Point clientPoint = this.bondsProvidersList.PointToClient(new Point(e.X, e.Y));
            // Get the row index of the item the mouse is below. 
            this.rowIndexOfItemUnderMouseToDrop =
            this.bondsProvidersList.HitTest(clientPoint.X, clientPoint.Y).RowIndex;
            // If the drag operation was a move then remove and insert the row.
            if (e.Effect == DragDropEffects.Move)
            {
                DataGridViewRow rowToMove = e.Data.GetData(
                typeof(DataGridViewRow)) as DataGridViewRow;
                this.bondsProvidersList.Rows.RemoveAt(this.rowIndexFromMouseDown);
                this.bondsProvidersList.Rows.Insert(this.rowIndexOfItemUnderMouseToDrop, rowToMove);
            }
        }

    }
}
