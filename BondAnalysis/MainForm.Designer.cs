﻿namespace BondAnalysis
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusBar = new System.Windows.Forms.ToolStripStatusLabel();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bondsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bondProvidersMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fieldsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolbarBonds = new System.Windows.Forms.ToolStripButton();
            this.toolbarBondProviders = new System.Windows.Forms.ToolStripButton();
            this.toolbarFields = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbarAbout = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbarHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.bondsTable = new System.Windows.Forms.DataGridView();
            this.infoTabControl = new System.Windows.Forms.TabControl();
            this.bondTab = new System.Windows.Forms.TabPage();
            this.bondInfoList = new System.Windows.Forms.ListView();
            this.bondFieldColumn = new System.Windows.Forms.ColumnHeader();
            this.bondValueColumn = new System.Windows.Forms.ColumnHeader();
            this.appIcons = new System.Windows.Forms.ImageList(this.components);
            this.bondTableTime = new System.Windows.Forms.Timer(this.components);
            this.bondProviderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusStrip.SuspendLayout();
            this.menu.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.toolStripContainer.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer.ContentPanel.SuspendLayout();
            this.toolStripContainer.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bondsTable)).BeginInit();
            this.infoTabControl.SuspendLayout();
            this.bondTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bondProviderBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBar});
            this.statusStrip.Location = new System.Drawing.Point(0, 0);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(890, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip";
            // 
            // statusBar
            // 
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(0, 17);
            this.statusBar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // menu
            // 
            this.menu.Dock = System.Windows.Forms.DockStyle.None;
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem,
            this.configurationToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(890, 24);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.exitMenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileMenuItem.Text = "&File";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(89, 6);
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitMenuItem.Text = "E&xit";
            this.exitMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bondsMenuItem,
            this.bondProvidersMenuItem,
            this.fieldsMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.configurationToolStripMenuItem.Text = "Settings";
            // 
            // bondsMenuItem
            // 
            this.bondsMenuItem.Name = "bondsMenuItem";
            this.bondsMenuItem.Size = new System.Drawing.Size(154, 22);
            this.bondsMenuItem.Text = "Bonds";
            this.bondsMenuItem.Click += new System.EventHandler(this.bondsMenuItem_Click);
            // 
            // bondProvidersMenuItem
            // 
            this.bondProvidersMenuItem.Name = "bondProvidersMenuItem";
            this.bondProvidersMenuItem.Size = new System.Drawing.Size(154, 22);
            this.bondProvidersMenuItem.Text = "Bond Providers";
            this.bondProvidersMenuItem.Click += new System.EventHandler(this.bondProvidersMenuItem_Click);
            // 
            // fieldsMenuItem
            // 
            this.fieldsMenuItem.Name = "fieldsMenuItem";
            this.fieldsMenuItem.Size = new System.Drawing.Size(154, 22);
            this.fieldsMenuItem.Text = "Fields";
            this.fieldsMenuItem.Click += new System.EventHandler(this.fieldsMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpMenuItem,
            this.toolStripSeparator4,
            this.aboutMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpMenuItem
            // 
            this.helpMenuItem.Name = "helpMenuItem";
            this.helpMenuItem.Size = new System.Drawing.Size(107, 22);
            this.helpMenuItem.Text = "&Help";
            this.helpMenuItem.Click += new System.EventHandler(this.helpMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(104, 6);
            // 
            // aboutMenuItem
            // 
            this.aboutMenuItem.Name = "aboutMenuItem";
            this.aboutMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutMenuItem.Text = "About";
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolbarBonds,
            this.toolbarBondProviders,
            this.toolbarFields,
            this.toolStripSeparator2,
            this.toolbarAbout,
            this.toolStripSeparator3,
            this.toolbarHelp});
            this.toolStrip.Location = new System.Drawing.Point(3, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(139, 25);
            this.toolStrip.TabIndex = 4;
            this.toolStrip.Text = "Main Toolbar";
            // 
            // toolbarBonds
            // 
            this.toolbarBonds.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarBonds.Image = ((System.Drawing.Image)(resources.GetObject("toolbarBonds.Image")));
            this.toolbarBonds.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarBonds.Name = "toolbarBonds";
            this.toolbarBonds.Size = new System.Drawing.Size(23, 22);
            this.toolbarBonds.Text = "Bonds";
            this.toolbarBonds.ToolTipText = "Configure the analyzed bonds.";
            this.toolbarBonds.Click += new System.EventHandler(this.bondsMenuItem_Click);
            // 
            // toolbarBondProviders
            // 
            this.toolbarBondProviders.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarBondProviders.Image = ((System.Drawing.Image)(resources.GetObject("toolbarBondProviders.Image")));
            this.toolbarBondProviders.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarBondProviders.Name = "toolbarBondProviders";
            this.toolbarBondProviders.Size = new System.Drawing.Size(23, 22);
            this.toolbarBondProviders.Text = "Bond Providers";
            this.toolbarBondProviders.ToolTipText = "Configure the bond providers.";
            this.toolbarBondProviders.Click += new System.EventHandler(this.bondProvidersMenuItem_Click);
            // 
            // toolbarFields
            // 
            this.toolbarFields.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarFields.Image = ((System.Drawing.Image)(resources.GetObject("toolbarFields.Image")));
            this.toolbarFields.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarFields.Name = "toolbarFields";
            this.toolbarFields.Size = new System.Drawing.Size(23, 22);
            this.toolbarFields.Text = "Fields";
            this.toolbarFields.ToolTipText = "Configure the displayed fields.";
            this.toolbarFields.Click += new System.EventHandler(this.fieldsMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbarAbout
            // 
            this.toolbarAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarAbout.Image = ((System.Drawing.Image)(resources.GetObject("toolbarAbout.Image")));
            this.toolbarAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarAbout.Name = "toolbarAbout";
            this.toolbarAbout.Size = new System.Drawing.Size(23, 22);
            this.toolbarAbout.Text = "About";
            this.toolbarAbout.ToolTipText = "About this application.";
            this.toolbarAbout.Click += new System.EventHandler(this.aboutMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbarHelp
            // 
            this.toolbarHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolbarHelp.Image")));
            this.toolbarHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarHelp.Name = "toolbarHelp";
            this.toolbarHelp.Size = new System.Drawing.Size(23, 22);
            this.toolbarHelp.Text = "Help";
            this.toolbarHelp.Click += new System.EventHandler(this.helpMenuItem_Click);
            // 
            // toolStripContainer
            // 
            // 
            // toolStripContainer.BottomToolStripPanel
            // 
            this.toolStripContainer.BottomToolStripPanel.Controls.Add(this.statusStrip);
            // 
            // toolStripContainer.ContentPanel
            // 
            this.toolStripContainer.ContentPanel.AutoScroll = true;
            this.toolStripContainer.ContentPanel.Controls.Add(this.splitContainer);
            this.toolStripContainer.ContentPanel.Size = new System.Drawing.Size(890, 393);
            this.toolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer.Name = "toolStripContainer";
            this.toolStripContainer.Size = new System.Drawing.Size(890, 464);
            this.toolStripContainer.TabIndex = 5;
            this.toolStripContainer.Text = "toolStripContainer";
            // 
            // toolStripContainer.TopToolStripPanel
            // 
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.menu);
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // splitContainer
            // 
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.bondsTable);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.infoTabControl);
            this.splitContainer.Size = new System.Drawing.Size(890, 393);
            this.splitContainer.SplitterDistance = 600;
            this.splitContainer.TabIndex = 0;
            // 
            // bondsTable
            // 
            this.bondsTable.AllowUserToAddRows = false;
            this.bondsTable.AllowUserToDeleteRows = false;
            this.bondsTable.AllowUserToOrderColumns = true;
            this.bondsTable.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bondsTable.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.bondsTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bondsTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.bondsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = "--";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.bondsTable.DefaultCellStyle = dataGridViewCellStyle2;
            this.bondsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bondsTable.GridColor = System.Drawing.Color.Silver;
            this.bondsTable.Location = new System.Drawing.Point(0, 0);
            this.bondsTable.Margin = new System.Windows.Forms.Padding(2);
            this.bondsTable.MultiSelect = false;
            this.bondsTable.Name = "bondsTable";
            this.bondsTable.ReadOnly = true;
            this.bondsTable.RowHeadersVisible = false;
            this.bondsTable.RowHeadersWidth = 20;
            this.bondsTable.RowTemplate.ReadOnly = true;
            this.bondsTable.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.bondsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.bondsTable.ShowRowErrors = false;
            this.bondsTable.Size = new System.Drawing.Size(598, 391);
            this.bondsTable.TabIndex = 0;
            this.bondsTable.VirtualMode = true;
            this.bondsTable.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.bondsTable_RowPrePaint);
            this.bondsTable.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.bondsTable_CellValueNeeded);
            this.bondsTable.SelectionChanged += new System.EventHandler(this.bondsTable_SelectionChanged);
            // 
            // infoTabControl
            // 
            this.infoTabControl.Controls.Add(this.bondTab);
            this.infoTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoTabControl.ImageList = this.appIcons;
            this.infoTabControl.ItemSize = new System.Drawing.Size(58, 32);
            this.infoTabControl.Location = new System.Drawing.Point(0, 0);
            this.infoTabControl.Name = "infoTabControl";
            this.infoTabControl.SelectedIndex = 0;
            this.infoTabControl.Size = new System.Drawing.Size(284, 391);
            this.infoTabControl.TabIndex = 0;
            // 
            // bondTab
            // 
            this.bondTab.Controls.Add(this.bondInfoList);
            this.bondTab.ImageKey = "money";
            this.bondTab.Location = new System.Drawing.Point(4, 36);
            this.bondTab.Name = "bondTab";
            this.bondTab.Padding = new System.Windows.Forms.Padding(3);
            this.bondTab.Size = new System.Drawing.Size(276, 351);
            this.bondTab.TabIndex = 0;
            this.bondTab.Text = "Bond Info";
            this.bondTab.ToolTipText = "Bond Information.";
            this.bondTab.UseVisualStyleBackColor = true;
            // 
            // bondInfoList
            // 
            this.bondInfoList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.bondFieldColumn,
            this.bondValueColumn});
            this.bondInfoList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bondInfoList.GridLines = true;
            this.bondInfoList.Location = new System.Drawing.Point(3, 3);
            this.bondInfoList.Name = "bondInfoList";
            this.bondInfoList.Size = new System.Drawing.Size(270, 345);
            this.bondInfoList.TabIndex = 0;
            this.bondInfoList.UseCompatibleStateImageBehavior = false;
            this.bondInfoList.View = System.Windows.Forms.View.Details;
            // 
            // bondFieldColumn
            // 
            this.bondFieldColumn.Text = "Field";
            this.bondFieldColumn.Width = 107;
            // 
            // bondValueColumn
            // 
            this.bondValueColumn.Text = "Value";
            this.bondValueColumn.Width = 103;
            // 
            // appIcons
            // 
            this.appIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("appIcons.ImageStream")));
            this.appIcons.TransparentColor = System.Drawing.Color.Fuchsia;
            this.appIcons.Images.SetKeyName(0, "main");
            this.appIcons.Images.SetKeyName(1, "money");
            this.appIcons.Images.SetKeyName(2, "field");
            this.appIcons.Images.SetKeyName(3, "bond-provider");
            this.appIcons.Images.SetKeyName(4, "save");
            this.appIcons.Images.SetKeyName(5, "exit");
            this.appIcons.Images.SetKeyName(6, "logo");
            this.appIcons.Images.SetKeyName(7, "help");
            // 
            // bondTableTime
            // 
            this.bondTableTime.Interval = 1000;
            this.bondTableTime.Tick += new System.EventHandler(this.bondTableTime_Tick);
            // 
            // bondProviderBindingSource
            // 
            this.bondProviderBindingSource.AllowNew = false;
            this.bondProviderBindingSource.DataSource = typeof(TCT.BondCollection);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 464);
            this.Controls.Add(this.toolStripContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.Name = "MainForm";
            this.Text = "Bond Analysis";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.toolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer.ContentPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.PerformLayout();
            this.toolStripContainer.ResumeLayout(false);
            this.toolStripContainer.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bondsTable)).EndInit();
            this.infoTabControl.ResumeLayout(false);
            this.bondTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bondProviderBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripContainer toolStripContainer;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.DataGridView bondsTable;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.TabControl infoTabControl;
        private System.Windows.Forms.TabPage bondTab;
        private System.Windows.Forms.ListView bondInfoList;
        private System.Windows.Forms.ColumnHeader bondFieldColumn;
        private System.Windows.Forms.ColumnHeader bondValueColumn;
        private System.Windows.Forms.ImageList appIcons;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bondsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bondProvidersMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fieldsMenuItem;
        private System.Windows.Forms.ToolStripButton toolbarBonds;
        private System.Windows.Forms.ToolStripButton toolbarBondProviders;
        private System.Windows.Forms.ToolStripButton toolbarFields;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolbarAbout;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem helpMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem aboutMenuItem;
        private System.Windows.Forms.ToolStripButton toolbarHelp;
        private System.Windows.Forms.ToolStripStatusLabel statusBar;
        public System.Windows.Forms.BindingSource bondProviderBindingSource;
        private System.Windows.Forms.Timer bondTableTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn cUSIPDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSINDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn askDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn askSizeDataGridViewTextBoxColumn;
    }
}

