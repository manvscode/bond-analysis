﻿namespace BondAnalysis
{
    partial class BondsProviderDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BondsProviderDlg));
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.bondsProvidersList = new System.Windows.Forms.DataGridView();
            this.bondsProvidersProviderColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bondsProvidersNotesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bondsProvidersList)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(244, 286);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(110, 28);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(360, 286);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(110, 28);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(12, 286);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(110, 28);
            this.addButton.TabIndex = 3;
            this.addButton.Text = "&Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(128, 286);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(110, 28);
            this.removeButton.TabIndex = 4;
            this.removeButton.Text = "&Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // bondsProvidersList
            // 
            this.bondsProvidersList.AllowDrop = true;
            this.bondsProvidersList.AllowUserToAddRows = false;
            this.bondsProvidersList.AllowUserToDeleteRows = false;
            this.bondsProvidersList.AllowUserToOrderColumns = true;
            this.bondsProvidersList.AllowUserToResizeRows = false;
            this.bondsProvidersList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bondsProvidersList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bondsProvidersProviderColumn,
            this.bondsProvidersNotesColumn});
            this.bondsProvidersList.Location = new System.Drawing.Point(12, 12);
            this.bondsProvidersList.Name = "bondsProvidersList";
            this.bondsProvidersList.RowHeadersVisible = false;
            this.bondsProvidersList.RowHeadersWidth = 20;
            this.bondsProvidersList.Size = new System.Drawing.Size(454, 268);
            this.bondsProvidersList.TabIndex = 5;
            this.bondsProvidersList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bondsProvidersList_MouseDown);
            this.bondsProvidersList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.bondsProvidersList_MouseMove);
            this.bondsProvidersList.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.bondsProvidersList_RowPrePaint);
            this.bondsProvidersList.DragOver += new System.Windows.Forms.DragEventHandler(this.bondsProvidersList_DragOver);
            this.bondsProvidersList.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.bondsProvidersList_CellEndEdit);
            this.bondsProvidersList.DragDrop += new System.Windows.Forms.DragEventHandler(this.bondsProvidersList_DragDrop);
            // 
            // bondsProvidersProviderColumn
            // 
            this.bondsProvidersProviderColumn.HeaderText = "Bond Provider";
            this.bondsProvidersProviderColumn.Name = "bondsProvidersProviderColumn";
            this.bondsProvidersProviderColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bondsProvidersProviderColumn.ToolTipText = "Bond Provider Short Code";
            this.bondsProvidersProviderColumn.Width = 160;
            // 
            // bondsProvidersNotesColumn
            // 
            this.bondsProvidersNotesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bondsProvidersNotesColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.bondsProvidersNotesColumn.HeaderText = "Notes";
            this.bondsProvidersNotesColumn.Name = "bondsProvidersNotesColumn";
            this.bondsProvidersNotesColumn.ToolTipText = "Notes about this bond provider.";
            // 
            // BondsProviderDlg
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(479, 322);
            this.Controls.Add(this.bondsProvidersList);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BondsProviderDlg";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bonds Providers";
            ((System.ComponentModel.ISupportInitialize)(this.bondsProvidersList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.DataGridView bondsProvidersList;
        private System.Windows.Forms.DataGridViewTextBoxColumn bondsProvidersProviderColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bondsProvidersNotesColumn;
    }
}
