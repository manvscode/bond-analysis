﻿/*
 * Copyright (C) 2011 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System.Runtime.InteropServices;
using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using BondAnalysis.AppData;
using BondAnalysis;
using System.Threading;

namespace TCT 
{
    public class Bloomberg
    {
        #region Variables
        protected IntPtr handle;
        private static Bloomberg instance = null;
        #endregion

        public struct Field
        {
            public const string Ask = "ASK";
            public const string AskSize = "ASK_SIZE";
            public const string Bid = "BID";
            public const string BidSize = "BID_SIZE";
            public const string CUSIP = "ID_CUSIP";
            public const string ISIN = "ID_ISIN";
        }

        public static Bloomberg Instance(  )
        {
            if( instance == null )
            {
                instance = new Bloomberg( BondAnalysis.Properties.Settings.Default.BloombergServer, BondAnalysis.Properties.Settings.Default.BloombergPort );
            }

            return instance;
        }

        protected Bloomberg( string server, ushort port )
        {
            this.handle = libblp.blp_create( server, port );
            Debug.Assert(handle.ToInt64() != 0);
        }

        ~Bloomberg( )
        {
            libblp.blp_destroy( this.handle );
        }

        #region Properties
        public IntPtr Handle
        {
            get
            {
                return this.handle;
            }
        }

        public ushort ErrorCode
        {
            get
            {
                return libblp.blp_error_code(this.handle);
            }
        }

        public string Error
        {
            get
            {
                return libblp.blp_error(this.handle);
            }
        }
        #endregion

        public static string FieldDescription(string field)
        {
            return libblp.blp_field_description(field);
        }


        #region Services
        public bool ReferenceData( string ticker, string[] fields, ref Security security )
        {
            security = new Security( );
            return libblp.blp_reference_data(this.handle, security.Handle, ticker, (uint) fields.Length, fields);
        }


        public bool MarketData( string[] securities, string[] fields, ref Subscription subscription )
        {
            subscription = new Subscription( );
            return libblp.blp_market_data(this.handle, subscription.Handle, securities, (uint) securities.Length, fields, (uint) fields.Length);
        }
        #endregion
    }

    public class Security
    {
        #region Variables
        protected IntPtr handle;
        protected bool ownsHandle;
        #endregion

        public Security()
        {
            this.handle = libblp.security_create();
            Debug.Assert(this.handle.ToInt64() != 0);
            this.ownsHandle = true;
        }

        public Security(IntPtr handle)
        {
            Debug.Assert(handle.ToInt64() != 0);
            this.handle = handle;
            this.ownsHandle = false;
        }

        public static Security FromMarketData(string ticker, string[] fields)
        {
            Bloomberg bb = Bloomberg.Instance();
            Security s = null;
            bb.ReferenceData(ticker, fields, ref s);
            Debug.Assert(s != null);
            return s;
        }

        ~Security()
        {
            if (this.ownsHandle)
            {
                libblp.security_destroy(this.handle);
                this.handle = new IntPtr(0L);
            }
        }

        #region Properties
        public IntPtr Handle
        {
            get
            {
                return this.handle;
            }

        }

        public string Ticker
        {
            get
            {
                string ticker = string.Empty;
                ticker = Marshal.PtrToStringAnsi(libblp.security_ticker(this.handle));
                return ticker;
            }
            set
            {
                libblp.security_set_ticker(this.handle, value);
            }
        }

        public int FieldCount
        {
            get
            {
                uint count = libblp.security_field_count(this.handle);
                return (int)count;
            }
        }
        #endregion
        #region Functions
        public bool HasField(string field)
        {
            bool has_field = false;
            has_field = libblp.security_has_field(this.handle, field);
            return has_field;
        }

        public object Field(string field)
        {
            ushort type = libblp.security_field_type(this.handle, field);
            object result = null;

            switch (type)
            {
                case libblp.BLP_FIELD_TYPE_DECIMAL:
                    {
                        double d = libblp.security_field_value_as_decimal(this.handle, field);
                        if (field.CompareTo("TCT_BEST_BID") == 0 || 
                            field.CompareTo("TCT_BEST_ASK") == 0 || 
                            field.CompareTo("BID") == 0 ||
                            field.CompareTo("ASK") == 0 )
                            result = d.ToString("$#,0.00");
                        else
                            result = d.ToString("#,0.000");
                        break;
                    }
                case libblp.BLP_FIELD_TYPE_INTEGER:
                    {
                        int n = libblp.security_field_value_as_integer(this.handle, field);
                        result = n.ToString("0,0");
                        break;
                    }
                case libblp.BLP_FIELD_TYPE_UNSIGNED_INTEGER:
                    {
                        uint n = libblp.security_field_value_as_uinteger(this.handle, field);
                        result = n.ToString("0,0");
                        break;
                    }
                case libblp.BLP_FIELD_TYPE_POINTER:
                    result = libblp.security_field_value_as_pointer(this.handle, field);
                    break;
                case libblp.BLP_FIELD_TYPE_STRING:
                    string str = Marshal.PtrToStringAnsi(libblp.security_field_value_as_string(this.handle, field));
                    result = str;
                    break;
                case libblp.BLP_FIELD_TYPE_NONE: // fall through
                    result = null;
                    break;
            }
            

			return result;
        }

		public bool AddOverride( string field, string val )
		{
            bool result = false;
            result = libblp.security_add_override(this.handle, field, val);
            return result;
		}

		public bool RemoveOverride( string field )
		{
            bool result = false;
            result = libblp.security_remove_override(this.handle, field);
            return result;
		}

		public bool HasOverride( string field )
		{
            bool result = false;
            result = libblp.security_has_override(this.handle, field);
            return result;
		}

		public void ClearOverrides( string field )
		{
            libblp.security_clear_overrides(this.handle);
        }
        #endregion
    }

    public class Subscription
    {
        protected IntPtr handle;
        protected bool ownsHandle;

        public Subscription()
        {
            this.handle = libblp.subscription_create();
            Debug.Assert(handle.ToInt64() != 0);
            this.ownsHandle = true;
        }

        public Subscription(IntPtr handle)
        {
            this.handle = handle;
            this.ownsHandle = false;
        }

        public static Subscription FromMarketData( string[] securities, string[] fields )
        {
            Bloomberg bb = Bloomberg.Instance();
            Subscription subscription = null;
            bb.MarketData( securities, fields, ref subscription );
            Debug.Assert(subscription != null);
            return subscription;
        }

        ~Subscription( )
        {
            if (this.ownsHandle)
            {
                libblp.subscription_destroy(this.handle);
                this.handle = new IntPtr(0L);
            }
        }

        #region Properties
        public IntPtr Handle
        {
            get
            {
                return this.handle;
            }
        }

		public bool IsTerminated
		{
			get
			{
				return libblp.subscription_is_terminated( this.handle );
			}
		}

        public double Interval
        {
            get
            {
                return libblp.subscription_interval(this.handle);
            }
            set
            {
                libblp.subscription_set_interval(this.handle, value);
            }
        }

        public int SecurityCount
        {
            get
            {
                return (int)libblp.subscription_security_count(this.handle);
            }
        }
        #endregion

        public bool End( )
        {
            return libblp.subscription_end(this.handle);
        }

        public bool HasSecurity(string ticker)
        {
            return libblp.subscription_has_security(this.handle, ticker);
        }

        public Security Security( string ticker )
        {
            IntPtr security_handle = libblp.subscription_security(this.handle, ticker);

            if (security_handle.ToInt64() == 0)
            {
                return null;
            }

            return new Security(security_handle);
        }

        public Security FirstSecurity()
        {
            IntPtr security_handle = libblp.subscription_first_security(this.handle);

            if (security_handle.ToInt64() == 0)
            {
                return null;
            }

            return new Security(security_handle);
        }


        public Security NextSecurity()
        {
            IntPtr security_handle = libblp.subscription_next_security(this.handle);

            if (security_handle.ToInt64() == 0)
            {
                return null;
            }

            return new Security(security_handle);
        }
    }

    public class Bond : Security
    {
        public Bond() : base()
        {
        }

        public Bond(IntPtr handle) : base(handle)
        {
            BackgroundWorker static_fields = new BackgroundWorker();
            static_fields.DoWork += this.request_mandatory_fields;
            static_fields.RunWorkerAsync(); 
        }

        ~Bond()
        {
        }

        protected void request_mandatory_fields(object sender, DoWorkEventArgs e)
        {
            string[] mandatoryFields = { "ID_CUSIP", "ID_ISIN", "SECURITY_DES" };
            List<string> fields = new List<string>();

            foreach (string field in mandatoryFields)
            {
                if (!this.HasField(field))
                {
                    fields.Add(field);
                }
            }

            if (fields.Count > 0)
            {
                libblp.blp_reference_data(Bloomberg.Instance().Handle, this.handle, this.Ticker, (uint)fields.Count, fields.ToArray());
            }

            e.Result = true;
        }

        public string this[string field]
        {
            get
            {
                object value = base.Field(field);
                return value == null ? string.Empty : (string) value;
            }
        }

        
        public string CUSIP
        {
            get
            {
                object value = base.Field(Bloomberg.Field.CUSIP);
                return value == null ? string.Empty : (string)value;
            }
        }

        public string ISIN
        {
            get
            {
                object value = base.Field(Bloomberg.Field.ISIN);
                return value == null ? string.Empty : (string)value;
            }
        }

        public double Bid
        {
            get
            {
                object value = base.Field(Bloomberg.Field.Bid);
                return value == null ? 0.0 : (double)value;
            }
        }

        public double Ask
        {
            get
            {
                object value = base.Field(Bloomberg.Field.Ask);
                return value == null ? 0.0 : (double)value;
            }
        }

        public uint BidSize
        {
            get
            {
                object value = base.Field(Bloomberg.Field.BidSize);
                return value == null ? 0 : (uint)value;
            }
        }

        public uint AskSize
        {
            get
            {
                object value = base.Field(Bloomberg.Field.AskSize);
                return value == null ? 0 : (uint)value;
            }
        }
    }

    public class BondCollection : Utility.ThreadedBindingList<Bond>//IList<Bond>
    {
        protected Subscription subscription;
        protected ArrayList tickers;


        public class BondEnumerator : IEnumerator<Bond>
        {
            protected IntPtr subscription;
            protected IntPtr currentBond;

            public BondEnumerator(IntPtr subscription) : base()
            {
                this.subscription = subscription;
                this.Reset();
            }

            public Bond Current
            {
                get
                {
                    return new Bond(this.currentBond);
                }
            }
            
            object IEnumerator.Current
            {
                get
                {
                    return new Bond(this.currentBond);
                }
            }

            public bool MoveNext()
            {
                this.currentBond = libblp.subscription_next_security(this.subscription);

                return this.currentBond.ToInt64() != 0;
            }

            public void Reset()
            {
                this.currentBond = libblp.subscription_first_security(this.subscription);
            }

            public void Dispose()
            {
            }
        }


        public BondCollection( ref Subscription subscription ) : base()
        {
            this.subscription = subscription;
            this.tickers = new ArrayList();

            // initialize tickers...

            Security security = subscription.FirstSecurity();
            while (security != null)
            {
                string ticker = security.Ticker;
                this.tickers.Add(ticker);
                security = subscription.NextSecurity();
            }
        }

        public int Count
        {
            get
            {
                return this.subscription.SecurityCount;
            }
        }

        public bool IsFixedSize
        {
            get
            {
                return true;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public bool IsSynchronized
        {
            get
            {
                // libbpl is not thread safe.
                return true;
            }
        }

        public object SyncRoot
        {
            get
            {
                return this.subscription;
            }
        }

        public Bond this[int index]
        {
            get
            {
                string ticker = (string) this.tickers[index];
                Security security = this.subscription.Security(ticker);
                return new Bond(security.Handle);
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        public Bond this[string ticker]
        {
            get
            {
                Debug.Assert(this.subscription.HasSecurity(ticker));
                Security security = this.subscription.Security(ticker);
                return new Bond( security.Handle );
            }
        }

        public bool Contains(Bond bond)
        {
            return this.subscription.HasSecurity(bond.Ticker);
        }

        public int IndexOf(Bond bond)
        {
            return this.tickers.IndexOf(bond.Ticker);
        }

        public void Add(Bond bond)
        {
            throw new NotSupportedException();
        }

        public void Insert(int index, Bond bond)
        {
            throw new NotSupportedException();
        }

        public bool Remove(Bond bond)
        {
            throw new NotSupportedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotSupportedException();
        }

        public void Clear()
        {
            throw new NotSupportedException();
        }

        public void CopyTo(Bond[] bonds, int index)
        {
            throw new NotSupportedException();
        }

        /*
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new BondEnumerator(this.subscription.Handle);
        }
        */

        public IEnumerator<Bond> GetEnumerator()
        {
            return new BondEnumerator(this.subscription.Handle);
        }
    }
}
