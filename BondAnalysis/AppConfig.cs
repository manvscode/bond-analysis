﻿/*
 * Copyright (C) 2011 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BondAnalysis.AppData
{
    public class AppConfig
    {
        public const string BONDS_XML = @"\bonds.xml";
        public const string BOND_PROVIDERS_XML = @"\bond-providers.xml";
        public const string FIELDS_XML = @"\fields.xml";

        public static List<BondConfig> LoadBonds()
        {
            string filename = Application.CommonAppDataPath + AppConfig.BONDS_XML;
            List<BondConfig> bonds = new List<BondConfig>();

            if (!File.Exists(filename))
            {
                SaveBonds(bonds);
            }

            XmlSerializer reader = new XmlSerializer(typeof(List<BondConfig>));
            StreamReader file = new StreamReader(filename);
            bonds = (List<BondConfig>)reader.Deserialize(file);
            file.Close();

            return bonds;
        }

        public static bool SaveBonds(List<BondConfig> bonds)
        {
            bool result = true;
            string filename = Application.CommonAppDataPath + AppConfig.BONDS_XML;

            if (!File.Exists(filename))
            {
                File.Delete(filename);
            }

            try
            {
                XmlSerializer writer = new XmlSerializer(typeof(List<BondConfig>));
                StreamWriter file = new StreamWriter(filename);
                writer.Serialize(file, bonds);
                file.Close();
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        public static List<BondProviderConfig> LoadBondProviders()
        {
            string filename = Application.CommonAppDataPath + AppConfig.BOND_PROVIDERS_XML;
            List<BondProviderConfig> bond_providers = new List<BondProviderConfig>();

            if (!File.Exists(filename))
            {
                SaveBondProviders(bond_providers);
            }

            XmlSerializer reader = new XmlSerializer(typeof(List<BondProviderConfig>));
            StreamReader file = new StreamReader(filename);
            bond_providers = (List<BondProviderConfig>)reader.Deserialize(file);
            file.Close();

            return bond_providers;
        }

        public static bool SaveBondProviders(List<BondProviderConfig> bond_providers)
        {
            bool result = true;
            string filename = Application.CommonAppDataPath + AppConfig.BOND_PROVIDERS_XML;

            if (!File.Exists(filename))
            {
                File.Delete(filename);
            }

            try
            {
                XmlSerializer writer = new XmlSerializer(typeof(List<BondProviderConfig>));
                StreamWriter file = new StreamWriter(filename);
                writer.Serialize(file, bond_providers);
                file.Close();
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        public static List<FieldConfig> LoadFields()
        {
            string filename = Application.CommonAppDataPath + AppConfig.FIELDS_XML;
            List<FieldConfig> fields = new List<FieldConfig>();

            if (!File.Exists(filename))
            {
                SaveFields(fields);
            }
            
            XmlSerializer reader = new XmlSerializer(typeof(List<FieldConfig>));
            StreamReader file = new StreamReader(filename);
            fields = (List<FieldConfig>)reader.Deserialize(file);
            file.Close();

            return fields;
        }

        public static bool SaveFields(List<FieldConfig> fields)
        {
            bool result = true;
            string filename = Application.CommonAppDataPath + AppConfig.FIELDS_XML;

            if (!File.Exists(filename))
            {
                File.Delete(filename);
            }

            try
            {
                XmlSerializer writer = new XmlSerializer(typeof(List<FieldConfig>));
                StreamWriter file = new StreamWriter(filename);
                writer.Serialize(file, fields);
                file.Close();
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
    }

    [Serializable()]
    public struct BondConfig
    {
        public string Bond;  // Could be a CUSIP, ISIN, et cetera.
        public string Notes;
    }

    [Serializable()]
    public struct BondProviderConfig
    {
        public string Provider;  // Could be a CUSIP, ISIN, et cetera.
        public string Notes;
    }

    [Serializable]
    public enum FieldType
    {
        Bloomberg = 0,
        TCT
    } 

    [Serializable]
    public struct FieldConfig
    {
        public FieldType Type;
        public string Mneumonic;
        public string Header;
    }
}
