﻿/*
 * Copyright (C) 2011 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BondAnalysis.AppData;

namespace BondAnalysis
{
    public partial class BondsDlg : Form
    {
        protected System.Collections.Generic.List<BondConfig> bonds;

        public BondsDlg()
        {
            InitializeComponent();
            this.bonds = new List<BondConfig>( );
        }

        public List<BondConfig> Bonds
        {
            get
            {
                return this.bonds;
            }
            set
            {
                this.bonds = value;
            }
        }

        new public DialogResult ShowDialog()
        {
            foreach (BondConfig bond in this.bonds)
            {
                this.addBondRow(bond);
            }

            DialogResult result = base.ShowDialog();

            this.bonds.Clear();

            foreach (DataGridViewRow row in this.bondsList.Rows)
            {
                if (row == null || row.Cells["bondsBondColumn"].Value == null) continue;

                BondConfig bc = new BondConfig();
                if( row.Cells["bondsBondColumn"].Value != null )
                    bc.Bond = row.Cells["bondsBondColumn"].Value.ToString();
                if (row.Cells["bondsNotesColumn"].Value != null)
                    bc.Notes = row.Cells["bondsNotesColumn"].Value.ToString();
                this.bonds.Add(bc);
            }

            return result;
        }

        protected void addBondRow(BondConfig bond)
        {
            //DataGridViewRow row = new DataGridViewRow();
            int rowIndex = this.bondsList.Rows.Add();
            DataGridViewRow row = this.bondsList.Rows[rowIndex];
            row.Cells["bondsBondColumn"].Value = bond.Bond;
            row.Cells["bondsNotesColumn"].Value = bond.Notes;
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in this.bondsList.SelectedCells)
            {
                try
                {
                    this.bondsList.Rows.RemoveAt(cell.RowIndex);
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            BondConfig bf = new BondConfig();
            bf.Bond = "** New Bond **";
            this.addBondRow(bf);
        }

        private void bondsList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                int columnIndex = e.ColumnIndex;
                int rowIndex = e.RowIndex;

                DataGridViewRow row = this.bondsList.Rows[rowIndex];

                string bond = string.Empty;

                if (row.Cells[columnIndex].Value != null)
                {
                    if (row.Cells[columnIndex] is DataGridViewTextBoxCell)
                    {
                        bond = row.Cells[columnIndex].Value.ToString().ToUpper();
                    }
                    else if (row.Cells[columnIndex] is DataGridViewComboBoxCell)
                    {
                        bond = ((DataGridViewComboBoxCell)row.Cells[columnIndex]).FormattedValue.ToString().ToUpper();
                    }
                }

                row.Cells[columnIndex].Value = bond; // made uppercase
            }
        }

        private void bondsList_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridViewRow row = this.bondsList.Rows[e.RowIndex];

            if (e.RowIndex % 2 == 0)
            {
                row.DefaultCellStyle.BackColor = Color.FromArgb(245, 245, 250);
            }
            else
            {
                row.DefaultCellStyle.BackColor = Color.White;
            }
        }


        private Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;

        private void bondsList_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (this.dragBoxFromMouseDown != Rectangle.Empty &&
                !this.dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = bondsList.DoDragDrop(
                    bondsList.Rows[this.rowIndexFromMouseDown],
                    DragDropEffects.Move);
                }
            }
        }

        private void bondsList_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            this.rowIndexFromMouseDown = bondsList.HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                // Remember the point where the mouse down occurred. 
                // The DragSize indicates the size that the mouse can move 
                // before a drag event should be started.                
                Size dragSize = SystemInformation.DragSize;
                // Create a rectangle using the DragSize, with the mouse position being
                // at the center of the rectangle.
                this.dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                e.Y - (dragSize.Height / 2)),
                dragSize);
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                this.dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void bondsList_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void bondsList_DragDrop(object sender, DragEventArgs e)
        {
            // The mouse locations are relative to the screen, so they must be 
            // converted to client coordinates.
            Point clientPoint = bondsList.PointToClient(new Point(e.X, e.Y));
            // Get the row index of the item the mouse is below. 
            this.rowIndexOfItemUnderMouseToDrop =
            bondsList.HitTest(clientPoint.X, clientPoint.Y).RowIndex;
            // If the drag operation was a move then remove and insert the row.
            if (e.Effect == DragDropEffects.Move)
            {
                DataGridViewRow rowToMove = e.Data.GetData(
                typeof(DataGridViewRow)) as DataGridViewRow;
                bondsList.Rows.RemoveAt(this.rowIndexFromMouseDown);
                bondsList.Rows.Insert(this.rowIndexOfItemUnderMouseToDrop, rowToMove);
            }
        }

    }
}
