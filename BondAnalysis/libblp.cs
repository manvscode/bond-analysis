﻿/*
 * Copyright (C) 2011 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System.Runtime.InteropServices;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

public class libblp
{
    #region Constants
    public const string BLP_DEFAULT_HOST                = "127.0.0.1";
    public const ushort BLP_DEFAULT_PORT                = 8194;
    public const ushort BLP_FIELD_TYPE_NONE             = 0;
    public const ushort BLP_FIELD_TYPE_STRING           = 1;
    public const ushort BLP_FIELD_TYPE_DECIMAL          = 2;
    public const ushort BLP_FIELD_TYPE_INTEGER          = 3;
    public const ushort BLP_FIELD_TYPE_UNSIGNED_INTEGER = 4;
    public const ushort BLP_FIELD_TYPE_POINTER          = 5;
    #endregion

    #region Bloomberg Library Functions
    [DllImport("libblp.dll", EntryPoint = "blp_create")]
    extern public static IntPtr blp_create( [MarshalAs(UnmanagedType.LPStr)] string server, ushort port );

    [DllImport("libblp.dll", EntryPoint = "blp_destroy")]
    extern public static void blp_destroy( IntPtr blp );

    [DllImport("libblp.dll", EntryPoint = "blp_error_code")]
    extern public static ushort blp_error_code( IntPtr blp );

    [DllImport("libblp.dll", EntryPoint = "blp_error")]
    [return: MarshalAs(UnmanagedType.LPStr)]
    extern public static string blp_error( IntPtr blp );

    [DllImport("libblp.dll", EntryPoint = "blp_field_mneumonic_by_index")]
    [return : MarshalAs(UnmanagedType.LPStr)]
    extern public static string blp_field_mneumonic_by_index(ushort index);

    [DllImport("libblp.dll", EntryPoint = "blp_field_description_by_index")]
    [return: MarshalAs(UnmanagedType.LPStr)]
    extern public static string blp_field_description_by_index(ushort index);

    [DllImport("libblp.dll", EntryPoint = "blp_field_description")]
    [return: MarshalAs(UnmanagedType.LPStr)]
    extern public static string blp_field_description( [MarshalAs(UnmanagedType.LPStr)] string field );
    #endregion

    #region Bloomberg Services Functions
    [DllImport("libblp.dll", EntryPoint = "blp_reference_data")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool blp_reference_data(IntPtr blp, IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string security_ticker, uint number_of_fields, [MarshalAs(UnmanagedType.LPArray)] string[] fields);

    [DllImport("libblp.dll", EntryPoint = "blp_market_data")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool blp_market_data(IntPtr blp, IntPtr subscription, [MarshalAs(UnmanagedType.LPArray)] string[] securities, uint number_of_securities, [MarshalAs(UnmanagedType.LPArray)] string[] fields, uint number_of_fields);
    #endregion

    #region Security Object Functions
    [DllImport("libblp.dll", EntryPoint = "security_create")]
    extern public static IntPtr security_create( );

    [DllImport("libblp.dll", EntryPoint = "security_destroy")]
    extern public static void security_destroy( IntPtr security );

    [DllImport("libblp.dll", EntryPoint = "security_ticker")]
    extern public static IntPtr security_ticker( IntPtr security );

    [DllImport("libblp.dll", EntryPoint = "security_set_ticker")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool security_set_ticker( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string ticker );

    [DllImport("libblp.dll", EntryPoint = "security_has_field")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool security_has_field( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field );

    [DllImport("libblp.dll", EntryPoint = "security_field_count")]
    extern public static uint security_field_count(IntPtr security);

    [DllImport("libblp.dll", EntryPoint = "security_field_type")]
    extern public static ushort security_field_type( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field );

    [DllImport("libblp.dll", EntryPoint = "security_field_value_as_string")]
    extern public static IntPtr security_field_value_as_string( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field );

    [DllImport("libblp.dll", EntryPoint = "security_field_value_as_decimal")]
    extern public static double security_field_value_as_decimal( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field );

    [DllImport("libblp.dll", EntryPoint = "security_field_value_as_integer")]
    extern public static int security_field_value_as_integer( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field );

    [DllImport("libblp.dll", EntryPoint = "security_field_value_as_uinteger")]
    extern public static uint security_field_value_as_uinteger( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field );

    [DllImport("libblp.dll", EntryPoint = "security_field_value_as_pointer")]
    extern public static IntPtr security_field_value_as_pointer(IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field);

    [DllImport("libblp.dll", EntryPoint = "security_field_buffer")]
    [return: MarshalAs(UnmanagedType.LPStr)]
    extern public static string security_field_buffer( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field );

    [DllImport("libblp.dll", EntryPoint = "security_set_field_buffer")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool security_set_field_buffer( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field, [MarshalAs(UnmanagedType.LPStr)] string value );

    [DllImport("libblp.dll", EntryPoint = "security_first_field")]
    [return: MarshalAs(UnmanagedType.LPStr)]
    extern public static string security_first_field( IntPtr security );

    [DllImport("libblp.dll", EntryPoint = "security_next_field")]
    [return: MarshalAs(UnmanagedType.LPStr)]
    extern public static string security_next_field( IntPtr security );

    [DllImport("libblp.dll", EntryPoint = "security_add_override")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool security_add_override( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field, [MarshalAs(UnmanagedType.LPStr)] string value );

    [DllImport("libblp.dll", EntryPoint = "security_remove_override")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool security_remove_override( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field );

    [DllImport("libblp.dll", EntryPoint = "security_has_override")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool security_has_override( IntPtr security, [MarshalAs(UnmanagedType.LPStr)] string field );

    [DllImport("libblp.dll", EntryPoint = "security_clear_overrides")]
    extern public static void security_clear_overrides( IntPtr security );
    #endregion

    #region Subscription Object Functions
    [DllImport("libblp.dll", EntryPoint = "subscription_create")]
    extern public static IntPtr subscription_create( );

    [DllImport("libblp.dll", EntryPoint = "subscription_destroy")]
    extern public static void subscription_destroy( IntPtr subscription );

    [DllImport("libblp.dll", EntryPoint = "subscription_end")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool subscription_end(IntPtr subscription);

    [DllImport("libblp.dll", EntryPoint = "subscription_is_terminated")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool subscription_is_terminated( IntPtr subscription );

    [DllImport("libblp.dll", EntryPoint = "subscription_interval")]
    extern public static double subscription_interval( IntPtr subscription );
    
    [DllImport("libblp.dll", EntryPoint = "subscription_set_interval")]
    extern public static void subscription_set_interval( IntPtr subscription, double interval );

    [DllImport("libblp.dll", EntryPoint = "subscription_has_security")]
    [return: MarshalAs(UnmanagedType.I1)]
    extern public static bool subscription_has_security(IntPtr subscription, [MarshalAs(UnmanagedType.LPStr)] string ticker);

    [DllImport("libblp.dll", EntryPoint = "subscription_security_count")]
    extern public static uint subscription_security_count(IntPtr subscription );

    [DllImport("libblp.dll", EntryPoint = "subscription_security")]
    extern public static IntPtr subscription_security(IntPtr subscription, [MarshalAs(UnmanagedType.LPStr)] string ticker);

    [DllImport("libblp.dll", EntryPoint = "subscription_first_security")]
    extern public static IntPtr subscription_first_security(IntPtr subscription);

    [DllImport("libblp.dll", EntryPoint = "subscription_next_security")]
    extern public static IntPtr subscription_next_security(IntPtr subscription);
    #endregion
}
