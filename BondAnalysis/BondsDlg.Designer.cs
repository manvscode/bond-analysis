﻿namespace BondAnalysis
{
    partial class BondsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BondsDlg));
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.bondsList = new System.Windows.Forms.DataGridView();
            this.bondsBondColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bondsNotesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bondsList)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(244, 286);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(110, 28);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(360, 286);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(110, 28);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(12, 286);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(110, 28);
            this.addButton.TabIndex = 3;
            this.addButton.Text = "&Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(128, 286);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(110, 28);
            this.removeButton.TabIndex = 4;
            this.removeButton.Text = "&Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // bondsList
            // 
            this.bondsList.AllowDrop = true;
            this.bondsList.AllowUserToAddRows = false;
            this.bondsList.AllowUserToDeleteRows = false;
            this.bondsList.AllowUserToOrderColumns = true;
            this.bondsList.AllowUserToResizeRows = false;
            this.bondsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bondsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bondsBondColumn,
            this.bondsNotesColumn});
            this.bondsList.Location = new System.Drawing.Point(12, 12);
            this.bondsList.Name = "bondsList";
            this.bondsList.RowHeadersVisible = false;
            this.bondsList.RowHeadersWidth = 20;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bondsList.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.bondsList.Size = new System.Drawing.Size(454, 268);
            this.bondsList.TabIndex = 5;
            this.bondsList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bondsList_MouseDown);
            this.bondsList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.bondsList_MouseMove);
            this.bondsList.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.bondsList_RowPrePaint);
            this.bondsList.DragOver += new System.Windows.Forms.DragEventHandler(this.bondsList_DragOver);
            this.bondsList.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.bondsList_CellEndEdit);
            this.bondsList.DragDrop += new System.Windows.Forms.DragEventHandler(this.bondsList_DragDrop);
            // 
            // bondsBondColumn
            // 
            this.bondsBondColumn.HeaderText = "Bond";
            this.bondsBondColumn.Name = "bondsBondColumn";
            this.bondsBondColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bondsBondColumn.ToolTipText = "The CUSIP, ISIN, et cetera.";
            this.bondsBondColumn.Width = 160;
            // 
            // bondsNotesColumn
            // 
            this.bondsNotesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bondsNotesColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.bondsNotesColumn.HeaderText = "Notes";
            this.bondsNotesColumn.Name = "bondsNotesColumn";
            this.bondsNotesColumn.ToolTipText = "Notes about this bond.";
            // 
            // BondsDlg
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(479, 322);
            this.Controls.Add(this.bondsList);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BondsDlg";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bonds";
            ((System.ComponentModel.ISupportInitialize)(this.bondsList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.DataGridView bondsList;
        private System.Windows.Forms.DataGridViewTextBoxColumn bondsBondColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bondsNotesColumn;
    }
}