﻿/*
 * Copyright (C) 2011 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BondAnalysis.AppData;
using TCT;
using System.Runtime.CompilerServices;

namespace BondAnalysis
{
    public partial class MainForm : Form
    {
        protected BondCollection bondCollection;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            toolStrip.ImageList = appIcons;
            exitMenuItem.Image = appIcons.Images["exit"];
            aboutMenuItem.Image = appIcons.Images["logo"];
            bondsMenuItem.Image = appIcons.Images["money"];
            bondProvidersMenuItem.Image = appIcons.Images["bond-provider"];
            fieldsMenuItem.Image = appIcons.Images["field"];
            helpMenuItem.Image = appIcons.Images["help"];

            toolbarBonds.Image = appIcons.Images["money"];
            toolbarBondProviders.Image = appIcons.Images["bond-provider"];
            toolbarFields.Image = appIcons.Images["field"];
            toolbarAbout.Image = appIcons.Images["logo"];
            toolbarHelp.Image = appIcons.Images["help"];


            this.bondCollection = new TCT.BondCollection(ref Program.BondsSubscription);

            this.bondTableTime.Enabled = true;
            this.refreshColumns();


            BackgroundWorker static_fields = new BackgroundWorker();
            static_fields.DoWork += this.requestBondData;
            static_fields.RunWorkerAsync(); 
        }

        protected void requestBondData(object sender, DoWorkEventArgs e)
        {
            while (!Program.BondsSubscription.IsTerminated )
            {
                libtct.compute_fields(Program.BondProvidersSubscription.Handle, Program.BondsSubscription.Handle);
            }

            e.Result = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutMenuItem_Click(object sender, EventArgs e)
        {
            AboutDlg dlg = new AboutDlg();

            dlg.ShowDialog();
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            aboutMenuItem_Click(sender, e);
        }

        private void fieldsMenuItem_Click(object sender, EventArgs e)
        {
            FieldsDlg dlg = new FieldsDlg();
            dlg.Fields = Program.Fields;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                AppConfig.SaveFields(dlg.Fields);
                this.refreshColumns();
            }
        }

        private void bondsMenuItem_Click(object sender, EventArgs e)
        {
            BondsDlg dlg = new BondsDlg();
            dlg.Bonds = Program.Bonds;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                AppConfig.SaveBonds(dlg.Bonds);
            }
        }

        private void bondProvidersMenuItem_Click(object sender, EventArgs e)
        {
            BondsProviderDlg dlg = new BondsProviderDlg();
            dlg.Providers = Program.BondProviders;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                AppConfig.SaveBondProviders(dlg.Providers);
            }

        }

        private void helpMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void bondsTable_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridViewRow row = this.bondsTable.Rows[e.RowIndex];

            if (e.RowIndex % 2 == 0)
            {
                row.DefaultCellStyle.BackColor = Color.FromArgb(245, 245, 250);
            }
            else
            {
                row.DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void bondTableTime_Tick(object sender, EventArgs e)
        {
            this.bondsTable.Refresh();
            this.bondsTable.Parent.Refresh();
        }

        protected void refreshColumns()
        {
            this.bondsTable.Columns.Clear();

            foreach (FieldConfig fc in Program.Fields)
            {
                string field_desc = libblp.blp_field_description(fc.Mneumonic);

                DataGridViewTextBoxColumn col = new DataGridViewTextBoxColumn();
                col.ReadOnly = true;
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                col.Name = fc.Mneumonic;
                //col.DataPropertyName = fc.Mneumonic;
                col.HeaderText = fc.Header;
                col.ToolTipText = field_desc;
                this.bondsTable.Columns.Add(col);
            }

            this.bondsTable.RowCount = this.bondCollection.Count;
        }

        private void bondProviderBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {

        }

        private void bondsTable_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {    
            // If this is the row for new records, no values are needed.
            if (e.RowIndex == this.bondsTable.RowCount - 1) return;

            Bond bond =  (Bond)this.bondCollection[e.RowIndex];          

            // Set the cell value to paint using the Customer object retrieved.
            string field = this.bondsTable.Columns[e.ColumnIndex].Name;

            if (bond.HasField(field))
            {
                object value = bond.Field(field);

                e.Value = value != null ? value : string.Empty;
            }
            else
            {
                e.Value = string.Empty;
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {

            Program.BondsSubscription.End();
            Program.BondProvidersSubscription.End();
        }


        private void bondsTable_SelectionChanged(object sender, EventArgs e)
        {
            if( this.bondsTable.SelectedRows.Count > 0 )
            {
                BackgroundWorker static_fields = new BackgroundWorker();
                static_fields.DoWork += this.requestBondInfoData;
                static_fields.RunWorkerCompleted += this.requestBondInfoDataCompleted;
                static_fields.RunWorkerAsync();
            }
        }
        protected static string[] bondInfoFields = { 
            "ID_CUSIP", 
            "ID_ISIN", 
            "CPN",
            "CPN_TYP",
            "CPN_CRNCY",
            "CPN_FREQ",
            "DAYS_TO_MTY",
        };

        protected void requestBondInfoData(object sender, DoWorkEventArgs e)
        {
            DataGridViewRow row = this.bondsTable.SelectedRows[0];
            string cusip = (string)row.Cells["ID_CUSIP"].FormattedValue;
            string isin = (string)row.Cells["ID_ISIN"].FormattedValue;

            Security bond = null;
            Bloomberg.Instance().ReferenceData(cusip + " Corp", MainForm.bondInfoFields, ref bond);
   
            e.Result = bond;
        }

        protected void requestBondInfoDataCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Security bond = (Security)e.Result;
            this.bondInfoList.Items.Clear();

            foreach (string field in MainForm.bondInfoFields)
            {
                ListViewItem item = new ListViewItem();
                item.Text = Bloomberg.FieldDescription(field);

                ListViewItem.ListViewSubItem subItem = new ListViewItem.ListViewSubItem();
                subItem.Text = (string)bond.Field(field);
                item.SubItems.Add(subItem);

                this.bondInfoList.Items.Add(item);
            }
        }
    }
}
