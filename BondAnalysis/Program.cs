﻿/*
 * Copyright (C) 2011 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using TCT;
using System;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using BondAnalysis.AppData;
using System.Collections;
using System.ComponentModel;
using System.Threading;


namespace BondAnalysis
{
    static class Program
    {
        public static List<BondConfig> Bonds;
        public static List<BondProviderConfig> BondProviders;
        public static List<FieldConfig> Fields;
        public static Bloomberg blp;
        public static Subscription BondsSubscription; // Real time Pricing Data
        public static Subscription BondProvidersSubscription;  // Pricing Data from Providers

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Program.Bonds         = AppConfig.LoadBonds();
            Program.BondProviders = AppConfig.LoadBondProviders();
            Program.Fields        = AppConfig.LoadFields();

            Program.blp = Bloomberg.Instance( );

            Program.BondsSubscription = Subscription.FromMarketData(Program.BondTickers(), Program.BondFields());
            Program.BondProvidersSubscription = Subscription.FromMarketData(Program.BondProviderTickers(), Program.BondProviderFields());


            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }



        public static string[] BondTickers()
        {
            List<string> result = new List<string>();

            if (Program.Bonds.Count > 0)
            {
                foreach (BondConfig bc in Program.Bonds)
                {
                    result.Add(bc.Bond + " Corp");
                }
            }

            return result.ToArray();
        }

        public static string[] BondFields()
        {
            List<string> result = new List<string>();

            foreach (FieldConfig fc in Program.Fields)
            {
                result.Add(fc.Mneumonic);
            }

            return result.ToArray();
        }

        public static string[] BondProviderTickers()
        {
            List<string> result = new List<string>();

            if (Program.Bonds.Count > 0)
            {
                foreach (BondConfig bc in Program.Bonds)
                {
                    foreach (BondProviderConfig bpc in Program.BondProviders)
                    {
                        result.Add(bc.Bond + "@" + bpc.Provider + " Corp");
                    }
                }
            }

            return result.ToArray();
        }


        public static string[] BondProviderFields()
        {
            List<string> result = new List<string>();
            result.Add("BID");
            result.Add("BID_SIZE");
            result.Add("ASK");
            result.Add("ASK_SIZE");

            return result.ToArray();
        }
    }
}
