﻿namespace BondAnalysis
{
    partial class FieldsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FieldsDlg));
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.fieldsList = new System.Windows.Forms.DataGridView();
            this.fieldsFieldColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldsHeaderColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldsDescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.fieldsList)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(320, 286);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(152, 28);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(474, 286);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(146, 28);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(12, 286);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(152, 28);
            this.addButton.TabIndex = 3;
            this.addButton.Text = "&Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(166, 286);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(152, 28);
            this.removeButton.TabIndex = 4;
            this.removeButton.Text = "&Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // fieldsList
            // 
            this.fieldsList.AllowDrop = true;
            this.fieldsList.AllowUserToAddRows = false;
            this.fieldsList.AllowUserToDeleteRows = false;
            this.fieldsList.AllowUserToOrderColumns = true;
            this.fieldsList.AllowUserToResizeRows = false;
            this.fieldsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fieldsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fieldsFieldColumn,
            this.fieldsHeaderColumn,
            this.fieldsDescriptionColumn});
            this.fieldsList.Location = new System.Drawing.Point(12, 12);
            this.fieldsList.Name = "fieldsList";
            this.fieldsList.RowHeadersVisible = false;
            this.fieldsList.RowHeadersWidth = 20;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.fieldsList.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.fieldsList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.fieldsList.Size = new System.Drawing.Size(609, 268);
            this.fieldsList.TabIndex = 5;
            this.fieldsList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.fieldsList_MouseDown);
            this.fieldsList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fieldsList_MouseMove);
            this.fieldsList.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.fieldsList_RowPrePaint);
            this.fieldsList.DragOver += new System.Windows.Forms.DragEventHandler(this.fieldsList_DragOver);
            this.fieldsList.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.fieldsList_CellEndEdit);
            this.fieldsList.DragDrop += new System.Windows.Forms.DragEventHandler(this.fieldsList_DragDrop);
            // 
            // fieldsFieldColumn
            // 
            this.fieldsFieldColumn.HeaderText = "Field";
            this.fieldsFieldColumn.Name = "fieldsFieldColumn";
            this.fieldsFieldColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fieldsFieldColumn.ToolTipText = "The Bloomberg field (i.e. BID, ASK, BID_SIZE, et cetera).";
            this.fieldsFieldColumn.Width = 80;
            // 
            // fieldsHeaderColumn
            // 
            this.fieldsHeaderColumn.HeaderText = "Header";
            this.fieldsHeaderColumn.Name = "fieldsHeaderColumn";
            this.fieldsHeaderColumn.ToolTipText = "The text that appears for the field\'s title.";
            this.fieldsHeaderColumn.Width = 158;
            // 
            // fieldsDescriptionColumn
            // 
            this.fieldsDescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.fieldsDescriptionColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.fieldsDescriptionColumn.HeaderText = "Field Description";
            this.fieldsDescriptionColumn.Name = "fieldsDescriptionColumn";
            this.fieldsDescriptionColumn.ReadOnly = true;
            this.fieldsDescriptionColumn.ToolTipText = "The description of the field from Bloomberg.";
            // 
            // FieldsDlg
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(633, 322);
            this.Controls.Add(this.fieldsList);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FieldsDlg";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bloomberg Fields Settings";
            ((System.ComponentModel.ISupportInitialize)(this.fieldsList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.DataGridView fieldsList;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldsFieldColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldsHeaderColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldsDescriptionColumn;
    }
}