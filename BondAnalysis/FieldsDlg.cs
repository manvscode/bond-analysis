﻿/*
 * Copyright (C) 2011 by Joseph A. Marrero and Shrewd LLC. http://www.manvscode.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BondAnalysis.AppData;

namespace BondAnalysis
{
    public partial class FieldsDlg : Form
    {
        protected const string UNKNOWN_FIELD = "Unknown field";
        protected List<AppData.FieldConfig> fields;

        public FieldsDlg()
        {
            InitializeComponent();
            this.fields = new List<AppData.FieldConfig>( );
            this.fieldsList.Columns["fieldsDescriptionColumn"].DefaultCellStyle.NullValue = FieldsDlg.UNKNOWN_FIELD;
        }

        public List<AppData.FieldConfig> Fields
        {
            get
            {
                return this.fields;
            }
            set
            {
                this.fields = value;
            }
        }

        new public DialogResult ShowDialog()
        {
            foreach (FieldConfig field in this.fields)
            {
                this.addFieldRow(field);
            }

            DialogResult result = base.ShowDialog();

            this.fields.Clear();

            foreach (DataGridViewRow row in this.fieldsList.Rows)
            {
                if (row == null || row.Cells["fieldsFieldColumn"].Value == null) continue;
                string field = (string)row.Cells["fieldsFieldColumn"].Value;

                FieldConfig fc = new FieldConfig();
                fc.Type = field.StartsWith("TCT_") ? FieldType.TCT : FieldType.Bloomberg;
                fc.Mneumonic = field;

                if (row.Cells["fieldsHeaderColumn"].Value != null)
                {
                    fc.Header = row.Cells["fieldsHeaderColumn"].Value.ToString();
                }
                this.fields.Add(fc);
            }


            return result;
        }

        protected void addFieldRow( FieldConfig field )
        {
            string description = FieldsDlg.UNKNOWN_FIELD;

            if (field.Type == FieldType.Bloomberg && field.Mneumonic.Length > 0)
            {
                description = TCT.Bloomberg.FieldDescription(field.Mneumonic);
            }
            else if (field.Type == FieldType.TCT && field.Mneumonic.Length > 0)
            {
                description = libtct.field_description(field.Mneumonic);
            }

            if (description == null) description = FieldsDlg.UNKNOWN_FIELD;

            //DataGridViewRow row = new DataGridViewRow();
            int rowIndex = this.fieldsList.Rows.Add();
            DataGridViewRow row = this.fieldsList.Rows[rowIndex];
            row.Cells["fieldsFieldColumn"].Value = field.Mneumonic;
            row.Cells["fieldsFieldColumn"].ToolTipText = "Click to edit";
            row.Cells["fieldsHeaderColumn"].Value = field.Header;
            row.Cells["fieldsDescriptionColumn"].Value = description;
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in this.fieldsList.SelectedCells)
            {
                try
                {
                    this.fieldsList.Rows.RemoveAt(cell.RowIndex);
                }
                catch (InvalidOperationException)
                {
                }
            }
        }


        private void addButton_Click(object sender, EventArgs e)
        {
            FieldConfig fc = new FieldConfig();
            fc.Type = FieldType.Bloomberg;
            fc.Mneumonic = "** New Item **";
            fc.Header = "New Item Header";
            this.addFieldRow(fc);
        }

        private void fieldsList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int columnIndex = e.ColumnIndex;
            int rowIndex = e.RowIndex;

            if (columnIndex == this.fieldsList.Rows[rowIndex].Cells["fieldsFieldColumn"].ColumnIndex)
            {
                DataGridViewRow row = this.fieldsList.Rows[rowIndex];
                string field = string.Empty;

                if (row.Cells[columnIndex].Value != null)
                {
                    if (row.Cells[columnIndex] is DataGridViewTextBoxCell)
                    {
                        field = row.Cells[columnIndex].Value.ToString().ToUpper();
                    }
                    else if (row.Cells[columnIndex] is DataGridViewComboBoxCell)
                    {
                        field = ((DataGridViewComboBoxCell)row.Cells[columnIndex]).FormattedValue.ToString().ToUpper();
                    }
                }

                string description = FieldsDlg.UNKNOWN_FIELD;

                if (field.StartsWith("TCT_"))
                {
                    description = libtct.field_description(field);
                }
                else
                {
                    description = TCT.Bloomberg.FieldDescription(field);
                }

                row.Cells["fieldsDescriptionColumn"].Value = description;
                row.Cells[columnIndex].Value = field; // made uppercase
            }
        }

        private void fieldsList_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridViewRow row = this.fieldsList.Rows[e.RowIndex];

            if (e.RowIndex % 2 == 0)
            {
                row.DefaultCellStyle.BackColor = Color.FromArgb(245, 245, 250);
            }
            else
            {
                row.DefaultCellStyle.BackColor = Color.White;
            }
        }


        private Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;

        private void fieldsList_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (this.dragBoxFromMouseDown != Rectangle.Empty &&
                !this.dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = fieldsList.DoDragDrop(
                    fieldsList.Rows[this.rowIndexFromMouseDown],
                    DragDropEffects.Move);
                }
            }
        }

        private void fieldsList_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            this.rowIndexFromMouseDown = fieldsList.HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                // Remember the point where the mouse down occurred. 
                // The DragSize indicates the size that the mouse can move 
                // before a drag event should be started.                
                Size dragSize = SystemInformation.DragSize;
                // Create a rectangle using the DragSize, with the mouse position being
                // at the center of the rectangle.
                this.dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                e.Y - (dragSize.Height / 2)),
                dragSize);
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                this.dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void fieldsList_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void fieldsList_DragDrop(object sender, DragEventArgs e)
        {
            // The mouse locations are relative to the screen, so they must be 
            // converted to client coordinates.
            Point clientPoint = fieldsList.PointToClient(new Point(e.X, e.Y));
            // Get the row index of the item the mouse is below. 
            this.rowIndexOfItemUnderMouseToDrop = fieldsList.HitTest(clientPoint.X, clientPoint.Y).RowIndex;
            // If the drag operation was a move then remove and insert the row.
            if (e.Effect == DragDropEffects.Move)
            {
                DataGridViewRow rowToMove = e.Data.GetData(
                typeof(DataGridViewRow)) as DataGridViewRow;
                fieldsList.Rows.RemoveAt(this.rowIndexFromMouseDown);
                fieldsList.Rows.Insert(this.rowIndexOfItemUnderMouseToDrop, rowToMove);
            }
        }

    }
}
